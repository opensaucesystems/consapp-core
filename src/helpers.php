<?php

if (! function_exists('proc_exec')) {
    /**
     * Execute command in shell and return output.
     * 
     * You will still need to escape the command being passed in.
     * 
     * @author Original Author <cbn@grenet.org>
     * 
     * @param  string   $cmd
     * @param  bool     $display_ouput
     * @param  int      $buffer_size
     * @return string
     */
    function proc_exec($cmd, $display_ouput = false, $buffer_size = 1024)
    {
        // stdin
        $FD_WRITE = 0;
        
        // stdout
        $FD_READ = 1;
        
        // stderr
        $FD_ERR = 2;
    
        $descriptorspec = [
            0 => ["pipe", "r"],
            1 => ["pipe", "w"],
            2 => ["pipe", "w"],
        ];
    
        $ptr = proc_open($cmd, $descriptorspec, $pipes, NULL, $_ENV);

        if (!is_resource($ptr)) {
            throw new Exception('ERROR: Not enough FD or out of memory. Failed to return a resource for the process: '.$cmd);
        }
    
        $stdout = '';
        $stderr = '';
    
        while (($buffer = fgets($pipes[$FD_READ], $buffer_size)) != NULL || ($errbuf = fgets($pipes[$FD_ERR], $buffer_size)) != NULL) {
            if (!isset($flag)) {
                $pstatus = proc_get_status($ptr);
                $first_exitcode = $pstatus["exitcode"];
                $flag = true;
            }

            if (strlen($buffer)) {
                if ($display_ouput) {
                    echo $buffer;
                }

                $stdout .= $buffer;
            }

            if (strlen($errbuf)) {
                if ($display_ouput) {
                    echo $errbuf;
                }

                $stderr .= $errbuf;
            }
        }
    
        foreach ($pipes as $pipe) {
            fclose($pipe);
        }
    
        /* Get the expected *exit* code to return the value */
        $pstatus = proc_get_status($ptr);

        if (!strlen($pstatus["exitcode"]) || $pstatus["running"]) {
            /* we can trust the retval of proc_close() */
            if ($pstatus["running"]) {
                proc_terminate($ptr);
            }

            $ret = proc_close($ptr);
        } else {
            if ((($first_exitcode + 256) % 256) == 255 && (($pstatus["exitcode"] + 256) % 256) != 255) {
                $ret = $pstatus["exitcode"];
            } elseif (!strlen($first_exitcode)) {
                $ret = $pstatus["exitcode"];
            } elseif ((($first_exitcode + 256) % 256) != 255) {
                $ret = $first_exitcode;
            } else {
                $ret = 0; /* we "deduce" an EXIT_SUCCESS ;) */
            }

            proc_close($ptr);
        }
        
        $exit_code = ($ret + 256) % 256;

        if ($exit_code == 127) {
            throw new Exception('Command not found (returned by sh).', 127);
        }
        
        if ($exit_code != 0) {
            throw new Exception($stderr, $exit_code);
        }

        return $stdout;
    }
}

if (! function_exists('env')) {
    /**
     * Gets the value of an environment variable. Supports boolean, empty and null.
     * 
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return $default;
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;

            case 'false':
            case '(false)':
                return false;

            case 'empty':
            case '(empty)':
                return '';

            case 'null':
            case '(null)':
                return;
        }

        return trim($value, '"');
    }
}

if (! function_exists('joinpath')) {
    /**
     * Join filesystem paths, it can take arrays and/or strings
     * 
     * @params mixed
     * @return string
     */
    function joinpath()
    {
        $paths = [];
        foreach (func_get_args() as $arg)
        {
            if ($arg !== '')
            {
                if (is_array($arg))
                {
                    foreach ($arg as $a)
                    {
                        $paths[] = $a;
                    }
                }
                else
                {
                    $paths[] = $arg;
                }
            }
        }
        return preg_replace('#/+#', '/', join('/', $paths));
    }
}
