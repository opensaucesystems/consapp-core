<?php

namespace Consapp;

use Phar;
use Consapp\Config;
use Dice\Dice;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\NotFoundException;
use Symfony\Component\Console\Application as Console;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Formatter\OutputFormatterInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Psr\Log\LoggerInterface;

class Application implements ContainerInterface
{
    /**
     * The Consapp version.
     *
     * @var string
     */
    const VERSION = '0.3.0';

    /**
     * The base path for the Consapp installation.
     *
     * @var string
     */
    protected $basePath;

    /**
     * Create a new Consapp application instance.
     *
     * @param  string|null  $basePath
     * @return void
     */
    public function __construct(Dice $dice, $basePath = null)
    {
        $this->dice = $dice;
        $this->registerCoreContainerRules();

        if ($basePath) {
            $this->setBasePath($basePath);
        }

        if ($this->isPhar()) {
            $this->setAbsPath(dirname(Phar::running(false)));
        } else {
            $this->setAbsPath($this->basePath());
        }

        $this->registerConfigs($this->configPath());
        $this->registerDatabase();
        $this->registerLogger();
    }

    /**
     * Get the version number of the application.
     *
     * @return string
     */
    public function version()
    {
        return static::VERSION;
    }

    /**
     * Set the absolute base path for the application.
     *
     * @param  string  $absPath
     * @return $this
     */
    public function setAbsPath($absPath)
    {
        $this->absPath = rtrim($absPath, '\/');

        return $this;
    }

    /**
     * Set the base path for the application.
     *
     * @param  string  $basePath
     * @return $this
     */
    public function setBasePath($basePath)
    {
        $this->basePath = rtrim($basePath, '\/');

        return $this;
    }

    /**
	 * Returns a fully constructed object based on $name using $args and $share as constructor arguments if supplied
	 * 
	 * @param string name The name of the class to instantiate
	 * @param array $args An array with any additional arguments to be passed into the constructor upon instantiation
	 * @param array $share Whether or not this class instance be shared, so that the same instance is passed around each time
	 * @return object A fully constructed object based on the specified input arguments
	 */
	public function create($name, array $args = [], array $share = [])
	{
        return $this->dice->create($name, $args, $share);
	}

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundException  No entry was found for this identifier.
     * @throws ContainerException Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get($id)
    {
        if ($this->has($id)) {
            return $this->dice->create($id);
        } else {
            throw new NotFoundException('Could not instantiate ' . $id);
        }
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundException`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return boolean
     */
    public function has($id)
    {
        return (class_exists($id) || $this->dice->getRule($id) != $this->dice->getRule('*'));
    }

    /**
     * Run console application.
     *
     * @return void
     */
    public function run()
    {
        $this->get(Console::class)->run();
    }

    /**
     * Check if application is a PHAR.
     *
     * @return bool
     */
    public function isPhar()
    {
        return !empty(Phar::running());
    }

    /**
     * Get the absolute path of the Consapp installation.
     * This will be the same even for a Phar.
     * 
     * @return string
     */
     public function absPath()
     {
         return $this->absPath;
     }

    /**
     * Get the path to the application "app" directory.
     *
     * @return string
     */
    public function path()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'app';
    }

    /**
     * Get the base path of the Consapp installation.
     *
     * @return string
     */
    public function basePath()
    {
        return $this->basePath;
    }

    /**
     * Get the path to the application configuration files.
     *
     * @return string
     */
    public function configPath()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'config';
    }

    /**
     * Register the core class rules in the container.
     *
     * @return void
     */
    public function registerCoreContainerRules()
    {
        $rules = [
            Command::class       => ['substitutions' => [LoggerInterface::class          => ['instance' => ConsoleLogger::class]]],
            ConsoleLogger::class => ['substitutions' => [OutputInterface::class          => ['instance' => ConsoleOutput::class]]],
            ConsoleOutput::class => ['substitutions' => [OutputFormatterInterface::class => ['instance' => OutputFormatter::class]]],
        ];

        foreach ($rules as $key => $rule) {
            $this->dice->addRule($key, $rule);
        }
    }

    /**
     * Register the config files.
     *
     * @return void
     */
    public function registerConfigs($path)
    {
        $this->config = $this->get(Config::class);
        $this->config->loadConfigurationFiles($path);
    }

    /**
     * Register the commands.
     * Pass an array of namespaced command classes or it will try and load them from the config file.
     *
     * @param  array $commands
     * @return void
     */
    public function registerCommands(array $commands = [])
    {
        if (empty($commands)) {
            $commands = $this->config->get('app.commands');
        }

        $rule = ['shared' => true];

        foreach ($commands as $command) {
            $rule['call'][] = ['add', [$this->get($command)]];
        }

        $this->dice->addRule(Console::class, $rule);
    }

    /**
     * Register the database.
     *
     * @return void
     */
    public function registerDatabase()
    {
        $rule = ['shared' => true];

        $this->dice->addRule(Database\Connector::class, $rule);
        $this->get(Database\Connector::class);
    }
    
    /**
     * Register the logger.
     *
     * @return void
     */
    public function registerLogger()
    {
        $rule = ['shared' => true];

        $this->dice->addRule(Log\Logger::class, $rule);
    }
}
