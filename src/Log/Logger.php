<?php

namespace Consapp\Log;

use Consapp\Application;
use Cascade\Cascade;

class Logger
{
    public function __construct(Application $app)
    {
        $this->app = $app;
        $config = $this->getConfig();
        $this->setConfig($config);
    }
    
    public function getConfig()
    {
        if (! $this->app->config->has('log.loggers')) {
            throw new \Exception('Config file for logger not found');
        }
        
        return $this->app->config->get('log');
    }
    
    public function setConfig($config)
    {
        Cascade::fileConfig($config);
    }
    
    public function get($channel)
    {
        return Cascade::getLogger($channel);
    }
}