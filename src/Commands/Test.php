<?php

namespace Consapp\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;

class Test extends Command
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('command:test')
            ->setDescription('Test command')
            ->setHelp('This is just a test')
            ->addOption(
                'option',
                'o',
                InputOption::VALUE_REQUIRED,
                'Option to test'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $option = $input->getOption('option');

        $msg = 'This is a test command.';

        if (!empty($option)) {
            $msg .= ' This is your option: '.$option;
        }

        $output->writeln($msg);
    }
}
