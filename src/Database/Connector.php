<?php

namespace Consapp\Database;

use Consapp\Application;
use Consapp\Configs;
use RedBeanPHP\R;

class Connector
{

    protected $app;

    /**
     * Connect database for the application.
     *
     * @param  \Consapp\Application  $app
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        $this->setModel();

        $this->createConnection();
    }

    public function createConnection()
    {
        $databaseConfig = $this->app->config->get('database');
        $default        = $databaseConfig['default'];
        $connection     = $databaseConfig['connections'][$default];
        $driver         = $connection['driver'];

        switch ($driver) {
            case 'mysql':
                $this->mysqlConnector($connection);
                break;

            case 'sqlite':
                $this->sqliteConnector($connection);
                break;
        }

        R::setAutoResolve(true);
    }

    public function mysqlConnector(array $connection)
    {
        // form the DSN from the current connection
        $dsn = $connection['driver'].':host='.$connection['host'].';dbname='.$connection['database'];

        // auth details
        $user = $connection['username'];
        $pass = $connection['password'];

        R::setup($dsn, $user, $pass);
    }

    public function sqliteConnector(array $connection)
    {
        if ($connection['database'] == basename($connection['database'])) {
            $db = joinpath($this->app->absPath(), $connection['database']);
        } else {
            $db = joinpath(realpath(dirname($connection['database'])), basename($connection['database']));
        }

        if (!is_dir(dirname($db))) {
            throw new \Exception("Path to database doesn't exist");
        }

        R::setup($connection['driver'].':'.$db);
    }

    public function setModel()
    {
        $model = $this->app->config->get('app.model');
        $model = trim($model, '\\');
        define('REDBEAN_MODEL_PREFIX', '\\'.$model.'\\');
    }
}
