<?php

namespace Consapp;

use Consapp\Config\Repository;
use Symfony\Component\Finder\Finder;

class Config extends Repository
{
    /**
     * The directory containing the config files
     *
     * @var string
     */
    protected $configPath;

    /**
     * Load the configuration items from all of the files.
     *
     * @param      $path
     */
    public function loadConfigurationFiles($path)
    {
        $this->configPath = $path;

        foreach ($this->getConfigurationFiles() as $fileKey => $path) {
            $this->set($fileKey, require $path);
        }
    }

    /**
     * Get the configuration files for the selected environment
     *
     * @return array
     */
    protected function getConfigurationFiles()
    {
        if (!is_dir($this->configPath)) {
            return [];
        }

        $files = [];
        foreach (Finder::create()->files()->name('*.php')->in($this->configPath)->depth(0) as $file) {
            $files[basename($file->getPathname(), '.php')] = $file->getPathname();
        }

        return $files;
    }
}
